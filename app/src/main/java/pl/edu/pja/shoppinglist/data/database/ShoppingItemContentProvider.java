package pl.edu.pja.shoppinglist.data.database;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by Piotr on 18.10.2016.
 */

public class ShoppingItemContentProvider extends ContentProvider {

    public static final String PACKAGE_NAME = "pl.edu.pja.shoppinglist";
    public static final String CONTENT_AUTHORITY = PACKAGE_NAME + ".provider";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_ITEM = "item";

    // Creates a UriMatcher object.
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_ITEM,
                           1);
        sUriMatcher.addURI(CONTENT_AUTHORITY, PATH_ITEM + "/#", 2);
    }

    private ShoppingItemsDatabaseDataSource source;

    @Override
    public boolean onCreate() {
        source = ShoppingItemsDatabaseDataSource.getInstance(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs,
                        String sortOrder) {
        switch (sUriMatcher.match(uri)) {
            case 1:
                return source.getDb().query(ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME,
                                            projection,
                                            selection,
                                            selectionArgs,
                                            null,
                                            null,
                                            sortOrder);
            case 2:
                long id = ContentUris.parseId(uri);
                return source.getDb().query(ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME,
                                            projection,
                                            ShoppingItemsPersistenceContract.ItemEntry._ID + " = ?",
                                            new String[]{String.valueOf(id)},
                                            null,
                                            null,
                                            sortOrder);
        }
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case 1:
                return ShoppingItemsPersistenceContract.ItemEntry.CONTENT_TYPE;
            case 2:
                return ShoppingItemsPersistenceContract.ItemEntry.CONTENT_ITEM_TYPE;
        }
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long id;
        Uri returnUri;
        switch (sUriMatcher.match(uri)) {
            case 1:

                id = source.getDb().insert(ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME,
                                           null,
                                           values);
                if (id > 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                    return ShoppingItemsPersistenceContract.ItemEntry.buildItemUri(id);
                }
                throw new UnsupportedOperationException("Unable to insert row into: " + uri);
            case 2:
            default:
        }
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        switch (sUriMatcher.match(uri)) {
            case 1:
                int rows = source.getDb().delete(
                        ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME,
                        selection,
                        selectionArgs);
                if (selection == null || rows != 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return rows;
            default:
        }
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        switch (sUriMatcher.match(uri)) {
            case 1:
                int rows = source.getDb().update(
                        ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs);
                if (rows != 0) {
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return rows;
            default:
        }
        throw new UnsupportedOperationException("Unknown uri: " + uri);
    }
}
