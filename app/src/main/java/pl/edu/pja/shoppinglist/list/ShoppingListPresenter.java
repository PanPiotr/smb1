package pl.edu.pja.shoppinglist.list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pl.edu.pja.shoppinglist.data.ShoppingItem;
import pl.edu.pja.shoppinglist.data.ShoppingItemRepository;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by pkuszewski on 08.10.2016.
 */

public class ShoppingListPresenter implements ShoppingListContract.Presenter {

    private static final String TAG = ShoppingListPresenter.class.getSimpleName();
    private ShoppingListContract.View view;
    private ShoppingItemRepository source;

    public ShoppingListPresenter(
            @NonNull Context context,
            @NonNull ShoppingListContract.View view
    ) {
        this.view = view;
        source = new ShoppingItemRepository(context);
    }


    @Override
    public void loadItems() {
        view.showLoadingView();
        source.getItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<ShoppingItem>>() {
                    @Override
                    public void call(List<ShoppingItem> shoppingItems) {
                        Collections.sort(shoppingItems, new Comparator<ShoppingItem>() {
                            @Override
                            public int compare(ShoppingItem o1, ShoppingItem o2) {
                                return o1.getName().compareTo(o2.getName());
                            }
                        });
                        view.showItems(shoppingItems);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        view.showErrorView();
                    }
                }, new Action0() {
                    @Override
                    public void call() {
                        view.hideLoadingView();
                    }
                });
    }

    @Override
    public void checkItem(@NonNull ShoppingItem item) {
        source.checkItem(item);
        Log.d(TAG, "checkItem: " + item.getName());
    }

    @Override
    public void uncheckItem(@NonNull ShoppingItem item) {
        source.uncheckItem(item);
        Log.d(TAG, "uncheckItem: " + item.getName());
    }

    @Override
    public void deleteItem(@NonNull ShoppingItem item) {
        Log.d(TAG, "deleteItem: " + item.getName());
        source.deleteItem(item);
        loadItems();
    }

    @Override
    public void addItem(@NonNull ShoppingItem item) {
        source.addItem(item);
        loadItems();
    }
}
