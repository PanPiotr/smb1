package pl.edu.pja.shoppinglist.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by pkuszewski on 08.10.2016.
 */

public class ShoppingListItemsDatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "items.db";
    public static final int DB_VERSION = 1;

    private static final String TEXT_TYPE = " TEXT";

    private static final String BOOLEAN_TYPE = " INTEGER";

    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME + " (" +
                    ShoppingItemsPersistenceContract.ItemEntry._ID + TEXT_TYPE + " PRIMARY KEY," +
                    ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_ID + TEXT_TYPE + COMMA_SEP +
                    ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_CHECKED + BOOLEAN_TYPE +
                    " )";

    public ShoppingListItemsDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
