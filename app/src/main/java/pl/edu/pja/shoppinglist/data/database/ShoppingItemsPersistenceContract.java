package pl.edu.pja.shoppinglist.data.database;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by pkuszewski on 08.10.2016.
 */

public class ShoppingItemsPersistenceContract {

    private ShoppingItemsPersistenceContract() {
    }

    public static abstract class ItemEntry implements BaseColumns {

        public static final Uri CONTENT_URI =
                ShoppingItemContentProvider.BASE_CONTENT_URI
                        .buildUpon()
                        .appendPath(ShoppingItemContentProvider.PATH_ITEM)
                        .build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/" + CONTENT_URI + "/" + ShoppingItemContentProvider.PATH_ITEM;
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/" + CONTENT_URI + "/" + ShoppingItemContentProvider.PATH_ITEM;

        public static final String TABLE_NAME = "shoppingitem";
        public static final String COLUMN_NAME_ID = "itemid";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_CHECKED = "is_checked";

        public static Uri buildItemUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
