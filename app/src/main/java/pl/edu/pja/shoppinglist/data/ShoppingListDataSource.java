package pl.edu.pja.shoppinglist.data;

import java.util.List;

import rx.Observable;

/**
 * Created by pkuszewski on 08.10.2016.
 */

public interface ShoppingListDataSource {

    void addItem(ShoppingItem item);

    Observable<List<ShoppingItem>> getItems();

    Observable<ShoppingItem> getItem(String itemId);

    void checkItem(ShoppingItem item);

    void uncheckItem(ShoppingItem item);

    void checkItem(String id);

    void uncheckItem(String id);

    void deleteItem(ShoppingItem item);

    void deleteItem(String itemId);

}
