package pl.edu.pja.shoppinglist.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by pkuszewski on 08.10.2016.
 */

@AllArgsConstructor(suppressConstructorProperties = true)
public class ShoppingItem {
    @Setter
    private String name;

    @Getter
    @Setter
    private String id;


    @Setter
    private boolean isChecked;

    public ShoppingItem() {

    }

    public boolean getIsChecked() {
        return this.isChecked;
    }

    public String getName() {
        return this.name;
    }
}
