package pl.edu.pja.shoppinglist.list;

import android.support.annotation.NonNull;

import java.util.List;

import pl.edu.pja.shoppinglist.data.ShoppingItem;

/**
 * Created by pkuszewski on 08.10.2016.
 */

public interface ShoppingListContract {

    interface View {
        void showItems(List<ShoppingItem> items);

        void showLoadingView();

        void hideLoadingView();

        void showNewItemUi();

        void showErrorView();
    }

    interface Presenter {

        void loadItems();

        void checkItem(@NonNull ShoppingItem item);

        void uncheckItem(@NonNull ShoppingItem item);

        void deleteItem(@NonNull ShoppingItem item);

        void addItem(@NonNull ShoppingItem item);
    }
}
