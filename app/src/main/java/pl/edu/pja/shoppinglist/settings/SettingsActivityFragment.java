package pl.edu.pja.shoppinglist.settings;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import pl.edu.pja.shoppinglist.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class SettingsActivityFragment extends PreferenceFragment {

    public SettingsActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
