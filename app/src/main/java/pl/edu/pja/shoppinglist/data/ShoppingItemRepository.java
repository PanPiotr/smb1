package pl.edu.pja.shoppinglist.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.List;

import pl.edu.pja.shoppinglist.R;
import pl.edu.pja.shoppinglist.data.database.ShoppingItemsDatabaseDataSource;
import pl.edu.pja.shoppinglist.data.firebase.FirebaseShoppingItemsDataSource;
import pl.edu.pja.shoppinglist.data.sharedprefs.SharedPrefsShoppingItemsDataSource;
import rx.Observable;

/**
 * Created by pkuszewski on 08.10.2016.
 */

public class ShoppingItemRepository implements ShoppingListDataSource {

    private ShoppingListDataSource source;

    private Context context;

    public ShoppingItemRepository(Context context) {
        this.context = context;
    }

    private void checkSource() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        ShoppingItemSource shoppingItemSource = ShoppingItemSource.valueOf(prefs.getString(context.getString(R.string.chosen_source_preference), ShoppingItemSource.SHARED_PREFS.toString()));
        switch (shoppingItemSource) {
            case SHARED_PREFS:
                source = SharedPrefsShoppingItemsDataSource.getInstance(context);
                break;
            case DATABASE:
                source = ShoppingItemsDatabaseDataSource.getInstance(context);
                break;
            case FIREBASE:
                source = FirebaseShoppingItemsDataSource.getInstance();
                break;
        }
    }

    @Override
    public void addItem(ShoppingItem item) {
        checkSource();
        source.addItem(item);
    }

    @Override
    public Observable<List<ShoppingItem>> getItems() {
        checkSource();
        return source.getItems();
    }

    @Override
    public Observable<ShoppingItem> getItem(String itemId) {
        checkSource();
        return source.getItem(itemId);
    }

    @Override
    public void checkItem(ShoppingItem item) {
        checkSource();
        source.checkItem(item);
    }

    @Override
    public void uncheckItem(ShoppingItem item) {
        checkSource();
        source.uncheckItem(item);
    }

    @Override
    public void checkItem(String id) {
        checkSource();
        source.checkItem(id);
    }

    @Override
    public void uncheckItem(String id) {
        checkSource();
        source.uncheckItem(id);
    }

    @Override
    public void deleteItem(ShoppingItem item) {
        boolean deletingAllowed = PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(context.getString(R.string.allow_delete_preference), true);
        if (deletingAllowed) {
            checkSource();
            source.deleteItem(item);
        }
    }

    @Override
    public void deleteItem(String itemId) {
        boolean deletingAllowed = PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(context.getString(R.string.allow_delete_preference), true);
        if (deletingAllowed) {
            checkSource();
            source.deleteItem(itemId);
        }
    }

}
