package pl.edu.pja.shoppinglist.list;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;
import lombok.Setter;
import pl.edu.pja.shoppinglist.R;
import pl.edu.pja.shoppinglist.data.ShoppingItem;
import pl.edu.pja.shoppinglist.databinding.ShoppingListItemBinding;

/**
 * Created by pkuszewski on 08.10.2016.
 */

public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.ViewHolder> {

    private static final String TAG = ShoppingListAdapter.class.getSimpleName();
    @Setter
    private List<ShoppingItem> items = new ArrayList<>();

    @Setter
    private ShoppingListActionsListener listener;

    @Setter
    private boolean deletingAllowed;


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ShoppingListItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.shopping_list_item, parent,
                false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ShoppingItem item = items.get(position);
        ShoppingListItemBinding binding = holder.getBinding();
        binding.setItem(item);
        binding.setDeletingAllowed(deletingAllowed);
        binding.setEventListener(this);
    }

    public void triggerCheckedAction(boolean isChecked, ShoppingItem item) {
        if (listener != null) {
            if (isChecked) {
                listener.onItemChecked(item);
            } else {
                listener.onItemUnckecked(item);
            }
        }
    }

    public void onDeleteButtonClicked(ShoppingItem item) {
        Log.d(TAG, "onDeleteButtonClicked: ");
        if (listener != null) {
            Log.d(TAG, "onClick: delete:" + item.getName());
            listener.onItemDeleted(item);
        }
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_name)
        TextView name;

        @BindView(R.id.item_checkbox)
        CheckBox checkBox;

        @BindView(R.id.item_delete_button)
        ImageView deleteButton;

        @Getter
        private ShoppingListItemBinding binding;

        ViewHolder(ShoppingListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            ButterKnife.bind(this, itemView);
        }
    }

    interface ShoppingListActionsListener {
        void onItemChecked(ShoppingItem item);

        void onItemUnckecked(ShoppingItem item);

        void onItemDeleted(ShoppingItem item);
    }
}
