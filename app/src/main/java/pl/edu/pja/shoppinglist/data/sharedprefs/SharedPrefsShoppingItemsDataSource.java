package pl.edu.pja.shoppinglist.data.sharedprefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.edu.pja.shoppinglist.data.ShoppingItem;
import pl.edu.pja.shoppinglist.data.ShoppingListDataSource;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by pkuszewski on 08.10.2016.
 */

public class SharedPrefsShoppingItemsDataSource implements ShoppingListDataSource {

    private static final String SHOPPING_ITEMS_SET = "shopping_items_set";
    public static final String SHOPPING_ITEMS_PREFERENCE_NAME = "shopping_items";

    private SharedPreferences prefs;

    private Gson gson;

    private static SharedPrefsShoppingItemsDataSource INSTANCE;

    public static SharedPrefsShoppingItemsDataSource getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SharedPrefsShoppingItemsDataSource(context);
        }
        return INSTANCE;
    }

    private SharedPrefsShoppingItemsDataSource(@NonNull Context context) {
        prefs = context.getSharedPreferences(SHOPPING_ITEMS_PREFERENCE_NAME, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    @Override
    public void addItem(ShoppingItem item) {
        Set<String> items = prefs.getStringSet(SHOPPING_ITEMS_SET, new HashSet<String>());
        items.add(gson.toJson(item));
        replaceAll(items);
    }

    @Override
    public Observable<List<ShoppingItem>> getItems() {
        return Observable.from(prefs.getStringSet(SHOPPING_ITEMS_SET, new HashSet<String>()))
                .map(new Func1<String, ShoppingItem>() {
                    @Override
                    public ShoppingItem call(String s) {
                        return gson.fromJson(s, ShoppingItem.class);
                    }
                }).toList();
    }

    @Override
    public Observable<ShoppingItem> getItem(final String itemId) {
        return getItems().flatMapIterable(new Func1<List<ShoppingItem>, Iterable<ShoppingItem>>() {
            @Override
            public Iterable<ShoppingItem> call(List<ShoppingItem> shoppingItems) {
                return shoppingItems;
            }
        }).filter(new Func1<ShoppingItem, Boolean>() {
            @Override
            public Boolean call(ShoppingItem item) {
                return item.getId().equals(itemId);
            }
        });
    }

    @Override
    public void checkItem(ShoppingItem item) {
        checkItem(item.getId());
    }

    @Override
    public void uncheckItem(ShoppingItem item) {
        uncheckItem(item.getId());
    }

    private void updateItem(final ShoppingItem itemParam) {
        getItems().flatMapIterable(new Func1<List<ShoppingItem>, Iterable<ShoppingItem>>() {
            @Override
            public Iterable<ShoppingItem> call(List<ShoppingItem> shoppingItems) {
                return shoppingItems;
            }
        }).map(new Func1<ShoppingItem, ShoppingItem>() {
            @Override
            public ShoppingItem call(ShoppingItem item) {
                if (item.getId().equals(itemParam.getId())) {
                    return itemParam;
                } else {
                    return item;
                }
            }
        }).map(new Func1<ShoppingItem, String>() {
            @Override
            public String call(ShoppingItem item) {
                return gson.toJson(item);
            }
        }).toList()
                .subscribe(new Action1<List<String>>() {
                    @Override
                    public void call(List<String> strings) {
                        replaceAll(strings);
                    }
                });
    }

    @Override
    public void checkItem(String id) {
        getItem(id)
                .subscribe(new Action1<ShoppingItem>() {
                    @Override
                    public void call(ShoppingItem item) {
                        item.setChecked(true);
                        updateItem(item);
                    }
                });
    }

    @Override
    public void uncheckItem(String id) {
        getItem(id)
                .subscribe(new Action1<ShoppingItem>() {
                    @Override
                    public void call(ShoppingItem item) {
                        item.setChecked(false);
                        updateItem(item);
                    }
                });
    }

    @Override
    public void deleteItem(final ShoppingItem item) {
        getItems().subscribe(new Action1<List<ShoppingItem>>() {
            @Override
            public void call(List<ShoppingItem> shoppingItems) {
                Set<String> target = new HashSet<>();
                for (ShoppingItem oldItem : shoppingItems) {
                    if (!item.getId().equals(oldItem.getId())) {
                        target.add(gson.toJson(oldItem));
                    }
                }
                replaceAll(target);
            }
        });
    }

    @Override
    public void deleteItem(String itemId) {
        getItem(itemId).subscribe(new Action1<ShoppingItem>() {
            @Override
            public void call(ShoppingItem item) {
                deleteItem(item);
            }
        });
    }

    private void replaceAll(List<String> items) {
        Set<String> itemSet = new HashSet<>();
        for (String item : items) {
            itemSet.add(item);
        }
        replaceAll(itemSet);
    }

    private void replaceAll(Set<String> items) {
        prefs.edit().putStringSet(SHOPPING_ITEMS_SET, items).commit();
    }
}
