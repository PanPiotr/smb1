package pl.edu.pja.shoppinglist.data;

/**
 * Created by pkuszewski on 08.10.2016.
 */

public enum ShoppingItemSource {
    SHARED_PREFS, DATABASE, FIREBASE
}
