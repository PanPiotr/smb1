package pl.edu.pja.shoppinglist.list;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Setter;
import pl.edu.pja.shoppinglist.R;
import pl.edu.pja.shoppinglist.data.ShoppingItem;
import pl.edu.pja.shoppinglist.databinding.ShoppingListItemBinding;

/**
 * A placeholder fragment containing a simple view.
 */
public class ShoppingListActivityFragment extends Fragment implements ShoppingListContract.View {

    @BindView(R.id.shopping_list_recycler)
    RecyclerView recycler;

    private ShoppingListAdapter adapter;

    @Setter
    private ShoppingListContract.Presenter presenter;

    private ShoppingListItemBinding biding;

    public ShoppingListActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping_list, container, false);
        ButterKnife.bind(ShoppingListActivityFragment.this, view);
        setupViews();
        return view;
    }

    private void setupViews() {
        adapter = new ShoppingListAdapter();
        recycler.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recycler.setAdapter(adapter);
        adapter.setListener(new ShoppingListAdapter.ShoppingListActionsListener() {
            @Override
            public void onItemChecked(ShoppingItem item) {
                presenter.checkItem(item);
            }

            @Override
            public void onItemUnckecked(ShoppingItem item) {
                presenter.uncheckItem(item);
            }

            @Override
            public void onItemDeleted(ShoppingItem item) {
                presenter.deleteItem(item);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.loadItems();
        adapter.setDeletingAllowed(PreferenceManager.getDefaultSharedPreferences(
                getActivity()).getBoolean(getString(R.string.allow_delete_preference), true));
    }

    @Override
    public void showItems(List<ShoppingItem> items) {
        adapter.setItems(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showLoadingView() {

    }

    @Override
    public void hideLoadingView() {

    }

    @Override
    public void showNewItemUi() {

    }

    @Override
    public void showErrorView() {

    }
}
