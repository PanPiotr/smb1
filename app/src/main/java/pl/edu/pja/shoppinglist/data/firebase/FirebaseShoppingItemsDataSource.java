package pl.edu.pja.shoppinglist.data.firebase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;
import com.kelvinapps.rxfirebase.RxFirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pja.shoppinglist.data.ShoppingItem;
import pl.edu.pja.shoppinglist.data.ShoppingListDataSource;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by pkuszewski on 19.11.2016.
 */

public class FirebaseShoppingItemsDataSource implements ShoppingListDataSource {

    private FirebaseDatabase firebaseDatabase;

    private static FirebaseShoppingItemsDataSource instance;

    private FirebaseShoppingItemsDataSource() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

    public static FirebaseShoppingItemsDataSource getInstance() {
        if (instance == null) {
            instance = new FirebaseShoppingItemsDataSource();
        }
        return instance;
    }

    @Override
    public void addItem(ShoppingItem item) {
        firebaseDatabase.getReference().child("items").child(item.getId()).setValue(item);
    }

    @Override
    public Observable<List<ShoppingItem>> getItems() {
        return RxFirebaseDatabase.observeValueEvent(
                firebaseDatabase.getReference().child("items"), new Func1<DataSnapshot, List<ShoppingItem>>() {
                    @Override
                    public List<ShoppingItem> call(DataSnapshot dataSnapshot) {
                        List<ShoppingItem> items = new ArrayList<ShoppingItem>();
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            ShoppingItem item = child.getValue(ShoppingItem.class);
                            items.add(item);
                        }
                        return items;
                    }
                });
    }

    @Override
    public Observable<ShoppingItem> getItem(String itemId) {
        return RxFirebaseDatabase.observeSingleValueEvent(
                firebaseDatabase.getReference().child("items").child(itemId), ShoppingItem.class);
    }

    @Override
    public void checkItem(ShoppingItem item) {
        item.setChecked(true);
        addItem(item);
    }

    @Override
    public void uncheckItem(ShoppingItem item) {
        item.setChecked(false);
        addItem(item);
    }

    @Override
    public void checkItem(String id) {
        getItem(id).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ShoppingItem>() {
                    @Override
                    public void call(ShoppingItem item) {
                        checkItem(item);
                    }
                });
    }

    @Override
    public void uncheckItem(String id) {
        getItem(id).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<ShoppingItem>() {
                    @Override
                    public void call(ShoppingItem item) {
                        uncheckItem(item);
                    }
                });
    }

    @Override
    public void deleteItem(ShoppingItem item) {
        firebaseDatabase.getReference().child("items").child(item.getId()).setValue(null);
    }

    @Override
    public void deleteItem(String itemId) {
        firebaseDatabase.getReference().child("items").child(itemId).setValue(null);
    }
}
