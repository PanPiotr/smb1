package pl.edu.pja.shoppinglist.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.List;

import lombok.Getter;
import pl.edu.pja.shoppinglist.data.ShoppingItem;
import pl.edu.pja.shoppinglist.data.ShoppingListDataSource;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by pkuszewski on 08.10.2016.
 */

public class ShoppingItemsDatabaseDataSource implements ShoppingListDataSource {

    private BriteDatabase rxdatabase;

    @Getter
    private SQLiteDatabase db;

    private Func1<Cursor, ShoppingItem> itemMapperFunction;

    private static ShoppingItemsDatabaseDataSource INSTANCE;

    public static ShoppingItemsDatabaseDataSource getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new ShoppingItemsDatabaseDataSource(context);
        }
        return INSTANCE;
    }

    private ShoppingItemsDatabaseDataSource(@NonNull Context context) {
        SqlBrite sqlBrite = SqlBrite.create();
        ShoppingListItemsDatabaseHelper helper = new ShoppingListItemsDatabaseHelper(context);
        rxdatabase = sqlBrite.wrapDatabaseHelper(helper, Schedulers.io());
        itemMapperFunction = new Func1<Cursor, ShoppingItem>() {
            @Override
            public ShoppingItem call(Cursor cursor) {
                String id = cursor.getString(cursor.getColumnIndexOrThrow(
                        ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_ID));
                boolean checked = cursor.getInt(cursor.getColumnIndexOrThrow(
                        ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_CHECKED)) == 1;
                String name = cursor.getString(cursor.getColumnIndexOrThrow(
                        ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_NAME));
                return new ShoppingItem(name, id, checked);
            }
        };
        db = helper.getWritableDatabase();
    }

    @Override
    public void addItem(ShoppingItem item) {
        ContentValues values = new ContentValues();
        values.put(ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_NAME, item.getName());
        values.put(ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_CHECKED,
                   item.getIsChecked());
        values.put(ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_ID, item.getId());
        rxdatabase.insert(ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME, values);
    }

    @Override
    public Observable<List<ShoppingItem>> getItems() {
        String[] projection = {
                ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_CHECKED,
                ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_ID,
                ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_NAME
        };
        String sql = String.format("SELECT %s FROM %s", TextUtils.join(",", projection),
                                   ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME);
        return rxdatabase.createQuery(ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME,
                                      sql).mapToList(itemMapperFunction);
    }

    @Override
    public Observable<ShoppingItem> getItem(String itemId) {
        String[] projection = {
                ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_CHECKED,
                ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_ID,
                ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_NAME
        };
        String sql = String.format("SELECT %s FROM %s WHERE %s LIKE ?",
                                   TextUtils.join(",", projection),
                                   ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME,
                                   ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_ID);
        return rxdatabase.createQuery(ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME,
                                      sql,
                                      itemId).mapToOneOrDefault(itemMapperFunction, null);
    }

    @Override
    public void checkItem(ShoppingItem item) {
        checkItem(item.getId());
    }

    @Override
    public void uncheckItem(ShoppingItem item) {
        uncheckItem(item.getId());
    }

    @Override
    public void checkItem(String id) {
        ContentValues values = new ContentValues();
        values.put(ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_CHECKED, true);
        String selection = ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_ID + " LIKE ?";
        String[] selectionArgs = {id};
        rxdatabase.update(ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME, values, selection,
                          selectionArgs);
    }

    @Override
    public void uncheckItem(String id) {
        ContentValues values = new ContentValues();
        values.put(ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_CHECKED, false);
        String selection = ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_ID + " LIKE ?";
        String[] selectionArgs = {id};
        rxdatabase.update(ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME, values, selection,
                          selectionArgs);
    }

    @Override
    public void deleteItem(ShoppingItem item) {
        deleteItem(item.getId());
    }

    @Override
    public void deleteItem(String itemId) {
        String selection = ShoppingItemsPersistenceContract.ItemEntry.COLUMN_NAME_ID + " LIKE ?";
        String[] selectionArgs = {itemId};
        rxdatabase.delete(ShoppingItemsPersistenceContract.ItemEntry.TABLE_NAME, selection,
                          selectionArgs);
    }
}
